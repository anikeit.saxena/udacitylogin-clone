import 'bootstrap/dist/css/bootstrap.min.css';
import { useForm } from "react-hook-form";
import { useRouter } from 'next/router';
import { userService } from '../helper/userService';

export default function Register() {
    const router = useRouter();
    
    const { register, handleSubmit, formState: { errors }} = useForm();
   
    const onSubmit = async (event) => {
        return userService
          .register(event.email, event.password)
          .then(() => {
            alert("Registration successful", {
              keepAfterRouteChange: true,
            });
            router.push("/");
          })
          .catch(() => {
            alert("Unable to register");
          });
      };


    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div >
                    <input className={errors.firstname && errors.firstname.type === "required" ? 'form-control form-control-lg my-2 is-invalid' : 'form-control form-control-lg my-2'} id='firstName' placeholder='First Name' type='text' name='firstName' style={{ fontSize: 16, padding: 14 }}
                        {...register("firstname", { required: true })}
                    />
                    {errors.firstname && errors.firstname.type === "required" &&
                        <div className='invalid-feedback'>First Name is required</div>}
                </div>
                <div>
                    <input className={errors.lastname && errors.lastname.type === "required" ? 'form-control form-control-lg my-2 is-invalid' : 'form-control form-control-lg my-2'} id='lastName' placeholder='Last Name' type='text' name='lastName' style={{ fontSize: 16, padding: 14 }}
                        {...register("lastname", { required: true })}
                    />
                    {errors.lastname && errors.lastname.type === "required" &&
                        <div className='invalid-feedback'>Last Name is required</div>}
                </div>
                <div>
                    <input className={errors.email && errors.email.type === "required" ? 'form-control form-control-lg my-2 is-invalid' : 'form-control form-control-lg my-2'} id='regEmail' placeholder='Email Address' type='text' name='regEmail' style={{ fontSize: 16, padding: 14 }}
                        {...register("email", { required: true })}
                    />
                    {errors.email && errors.email.type === "required" &&
                        <div className='invalid-feedback'>Email Address is required</div>}
                </div>
                <div>
                    <div className='input-group' style={{display: 'flex', flexDirection:'column'}}>
                        <div>
                        <input className={errors.password && errors.password.type === "required"? 'form-control form-control-lg my-2 is-invalid' : 'form-control form-control-lg my-2'} id='regPassword' placeholder='Password' type='password' name='regPassword' style={{ fontSize: 16, padding: 14 }}
                            {...register("password", { required: true, minLength: 6})}
                        />
                        </div>
                        {errors.password && errors.password.type === "required" &&
                            <div className='text-danger'><small>Password is required</small></div>}
                        {errors.password && errors.password.type === "minLength" &&
                            <div className='text-danger'><small>Password length must be atleast 6 characters</small></div>}    
                    </div>
                </div>
                <div className='mb-4' style={{ fontSize: 14, color: 'rgb(109, 119, 128)' }}>
                    <span> By Clicking on Sign up, you agree to our <a href='https://www.udacity.com/legal/terms-of-service' style={{textDecoration: 'none', fontWeight: 500}}> Terms of Use </a> and our <a href='https://www.udacity.com/legal/privacy' style={{textDecoration: 'none', fontWeight: 500}}> Privacy Policy </a> </span>
                </div>
                <div className='col-md-4 text-center w-100'>
                    <button type='submit' className='btn btn-primary btn-lg shadow-sm p-2 rounded' style={{ backgroundColor: "rgb(1, 122, 155)" }}>
                        <p className='m-0 px-2 py-1' style={{ fontSize: 14, fontWeight: 500, letterSpacing: 2 }}>SIGN UP</p>
                    </button>
                </div>
            </form>
        </div>
    )
}