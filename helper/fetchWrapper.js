import { userService } from './userService';

export const fetchWrapper = {
  get,
  post,
};

function get(url) {
  const requestOptions = {
    method: "GET",
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function post(url, body) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

export function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if ([400, 401, 403].includes(response.status) && userService.userValue) {
        userService.logout();
      }
      return Promise.reject(data.error);
    }
    return data.token;
  });
}
