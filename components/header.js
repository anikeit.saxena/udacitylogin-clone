import Image from 'next/image'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Header(){
    return(
        <div className='shadow py-4 mb-4 bg-white rounded row justify-content-center align-middle' style={{height: 75}}>
            <Image src='/udacitylogo.svg' height={32} width={180}/>
        </div>
    )
}