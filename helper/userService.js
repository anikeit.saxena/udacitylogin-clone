import { BehaviorSubject } from "rxjs";
import Router from "next/router";

import { fetchWrapper } from "./fetchWrapper";

const baseUrl = "https://reqres.in/api";
export const userSubject = new BehaviorSubject(null);

export const userService = {
  user: userSubject.asObservable(),
  get userValue() {
    return userSubject.value;
  },
  login,
  logout,
  register,
};

function login(username, password) {
  return fetchWrapper
    .post(`${baseUrl}/login`, {
      email: username,
      password: password,
    })
    .then((user) => {
      userSubject.next(user);
      return user;
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function logout() {
  userSubject.next(null);
  Router.push("/");
}

function register(username, password) {
  return fetchWrapper
    .post(`${baseUrl}/register`, {
      email: username,
      password: password,
    })
    .then((user) => {
      return user;
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}