import Image from "next/image";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Google({toggle}) {
    return (
        <div className='col-sm text-center'>
            <button type='button' className='btn btn-lg btn-block shadow-sm bg-white rounded ' style={{ width: '100%' }}>
                <div className='container d-flex p-0 justify-content-center'>

                    <Image src='/google.svg' width={40} height={40} />

                    {toggle === 1 && <div className='align-self-center mx-1' style={{ fontSize: 14, fontWeight: 500, color: 'rgb(109, 119, 128)', alignSelf: 'center' }}>
                        Sign up with Google
                    </div>}
                    {toggle === 2 && <div className='align-self-center mx-1' style={{ fontSize: 14, fontWeight: 500, color: 'rgb(109, 119, 128)', alignSelf: 'center' }}>
                        Sign in with Google
                    </div>}
                </div>
            </button>
        </div>
    );
}